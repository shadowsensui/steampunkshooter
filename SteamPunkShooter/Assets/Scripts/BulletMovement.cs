﻿using UnityEngine;
using System.Collections;

public class BulletMovement : MonoBehaviour {

	public float Speed;

	Rigidbody2D RB2D;

	// Use this for initialization
	void Start () 
	{
		RB2D = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		RB2D.velocity = new Vector2 (4.0f, 0.0f) * Speed;
	}
}
