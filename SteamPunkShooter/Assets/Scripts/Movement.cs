﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {

	public float Speed;
	public Transform SpawnPoint;
	public GameObject Projectile;

	Rigidbody2D RB2D;

	// Use this for initialization
	void Start () 
	{
		RB2D = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Vector2 movement = new Vector2 (moveHorizontal, moveVertical);
		RB2D.velocity = movement * Speed;

		if (Input.GetKeyDown("space"))
		{
			Instantiate (Projectile, SpawnPoint.position, Quaternion.identity);
		}
	}
}
